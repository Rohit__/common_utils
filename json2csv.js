var jsonToCSV = require('json-to-csv');
const fileName = './Transactions.csv';
const o = [{
  "TransactionId" : "IN00983587",
  "latestResponse" : {
      "response" : {
          "sanima_response" : {
              "AGENT_TXNID" : "IN00983587",
              "STATUS_DATE" : "",
              "STATUS" : "UN-PAID",
              "PAYOUTCURRENCY" : "NPR",
              "PAYOUTAMT" : "1687740.0800",
              "RECEIVER_NAME" : "PREM RANA",
              "SENDER_NAME" : "PREM RANA",
              "PINNO" : "44112048500",
              "MESSAGE" : "TXN Summary",
              "AGENT_SESSION_ID" : "IN00983587",
              "CODE" : "0"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN01270938",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8fce56846590939226e",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:50:04.748Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.209Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e30"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.593Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e42"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "014",
                  "paymentReferenceNumber" : "IN01270938",
                  "purposeDescription" : "Gift - Family Maintenance",
                  "receiverMobileNumber" : "66637615466",
                  "receiverEmail" : "honey19061983@gmail.com",
                  "currency" : "THB",
                  "amount" : "6376.87",
                  "receiverAccountNumber" : "8032065058",
                  "receiverBankName" : "Siam Commercial Bank Public Company Limited (SCB)",
                  "receiverName" : "Malisoon Chaisang",
                  "orderingCustomerIdentificationNumber" : "G3043724T",
                  "orderingCustomerName" : "PRAKASH SANKARAN"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.593Z",
              "requestCreatedTimestamp" : "2018-09-21T12:50:04.748Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN01270938",
              "_id" : "5ba4e8fce56846590939226d"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN02195426",
  "latestResponse" : {
      "response" : {
          "sanima_response" : {
              "AGENT_TXNID" : "IN02195426",
              "STATUS_DATE" : "",
              "STATUS" : "UN-PAID",
              "PAYOUTCURRENCY" : "NPR",
              "PAYOUTAMT" : "1264634.9900",
              "RECEIVER_NAME" : "PADAM BAHADUR SHRESTHA",
              "SENDER_NAME" : "BHIM SHRESTHA",
              "PINNO" : "44111259495",
              "MESSAGE" : "TXN Summary",
              "AGENT_SESSION_ID" : "IN02195426",
              "CODE" : "0"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN02241558",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "On Hold",
              "cash_out_status" : 5,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN17347815",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8f4e568465909392266",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:56.726Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.201Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e28"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.589Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3e"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "017",
                  "paymentReferenceNumber" : "IN17347815",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "66812664213",
                  "receiverEmail" : "btlau@hotmail.com",
                  "currency" : "THB",
                  "amount" : "77897.16",
                  "receiverAccountNumber" : "5401033337",
                  "receiverBankName" : "Citibank N.A. (CITI)",
                  "receiverName" : "Lau Boon Tuan",
                  "orderingCustomerIdentificationNumber" : "690507045453",
                  "orderingCustomerName" : "Lau Boon Kok"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.589Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:56.726Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN17347815",
              "_id" : "5ba4e8f4e568465909392265"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN22296026",
  "latestResponse" : {
      "response" : {
          "sanima_response" : {
              "AGENT_TXNID" : "IN22296026",
              "STATUS_DATE" : "",
              "STATUS" : "UN-PAID",
              "PAYOUTCURRENCY" : "NPR",
              "PAYOUTAMT" : "2628018.9700",
              "RECEIVER_NAME" : "RAM CHANDRA GHALE",
              "SENDER_NAME" : "DHAN MAN GHALE",
              "PINNO" : "44113216991",
              "MESSAGE" : "TXN Summary",
              "AGENT_SESSION_ID" : "IN22296026",
              "CODE" : "0"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN23181169",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "Delivered",
              "cash_out_status" : 4,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN23819845",
  "latestResponse" : {
      "response" : {
          "data" : {
              "mapped_status" : "ERROR",
              "datainfo" : "Suspicious Transaction",
              "message" : "FAILED",
              "status" : 220
          },
          "success" : false
      }
  }
}

,
{
  "TransactionId" : "IN27398200",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "Received",
              "cash_out_status" : 0,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN27587845",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8f8e56846590939226a",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:50:00.734Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.205Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e2c"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.591Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e40"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "004",
                  "paymentReferenceNumber" : "IN27587845",
                  "purposeDescription" : "Gift - Family Maintenance",
                  "receiverMobileNumber" : "66923414218",
                  "receiverEmail" : "Tontoey.wachiranan@gmail.com",
                  "currency" : "THB",
                  "amount" : "25000",
                  "receiverAccountNumber" : "0431833379",
                  "receiverBankName" : "Kasikorn Bank Public Company Limited (KBANK)",
                  "receiverName" : "Vitchanart Komkris",
                  "orderingCustomerIdentificationNumber" : "G3321816N",
                  "orderingCustomerName" : "Wachiranan Makasira"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.591Z",
              "requestCreatedTimestamp" : "2018-09-21T12:50:00.734Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN27587845",
              "_id" : "5ba4e8f8e568465909392269"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN28758750",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba45122e5684659093920fa",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T02:02:10.633Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T03:00:59.786Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092102",
                      "_id" : "5ba45eebe5684659093920fb"
                  }, 
                  {
                      "timestamp" : "2018-09-21T03:01:04.686Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092102",
                      "_id" : "5ba45ef0e5684659093920fe"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "002",
                  "paymentReferenceNumber" : "IN28758750",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "6596200622",
                  "receiverEmail" : "mtth3w@gmail.com",
                  "currency" : "THB",
                  "amount" : "35000",
                  "receiverAccountNumber" : "1019165917",
                  "receiverBankName" : "Bangkok Bank Public Company Limited (BBL)",
                  "receiverName" : "chung sheng lim",
                  "orderingCustomerIdentificationNumber" : "s8410583Z",
                  "orderingCustomerName" : "sheng chung lim"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T03:01:04.686Z",
              "requestCreatedTimestamp" : "2018-09-21T02:02:10.633Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN28758750",
              "_id" : "5ba45122e5684659093920f9"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN29403126",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8f0e568465909392262",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:52.780Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.197Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e24"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.587Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3c"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "030",
                  "paymentReferenceNumber" : "IN29403126",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "66979700935",
                  "receiverEmail" : "Achara530110496@gmail.com",
                  "currency" : "THB",
                  "amount" : "23536.73",
                  "receiverAccountNumber" : "020233592607",
                  "receiverBankName" : "Government Saving Bank (GOV)",
                  "receiverName" : "Achara Kratang",
                  "orderingCustomerIdentificationNumber" : "G3285659X",
                  "orderingCustomerName" : "Kratang Achara"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.587Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:52.780Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN29403126",
              "_id" : "5ba4e8f0e568465909392261"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN30146927",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8fae56846590939226c",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:50:02.741Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.207Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e2e"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.592Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e41"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "014",
                  "paymentReferenceNumber" : "IN30146927",
                  "purposeDescription" : "Gift - Family Maintenance",
                  "receiverMobileNumber" : "60122111386",
                  "receiverEmail" : "ladymafia.cool@gmail.com",
                  "currency" : "THB",
                  "amount" : "7805.05",
                  "receiverAccountNumber" : "1642383226",
                  "receiverBankName" : "Siam Commercial Bank Public Company Limited (SCB)",
                  "receiverName" : "Wisansaya Kaeowiset",
                  "orderingCustomerIdentificationNumber" : "731004145215",
                  "orderingCustomerName" : "Nicholas Wong Yew Seong"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.592Z",
              "requestCreatedTimestamp" : "2018-09-21T12:50:02.741Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN30146927",
              "_id" : "5ba4e8fae56846590939226b"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN41951002",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8eee568465909392260",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:50.690Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.195Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e22"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.586Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3b"
                  }
              ],
              "store" : {
                  "purposeCode" : "318024",
                  "fiCode" : "014",
                  "paymentReferenceNumber" : "IN41951002",
                  "purposeDescription" : "Construction costs/expenses",
                  "receiverMobileNumber" : "66819203262",
                  "receiverEmail" : "Pong3312004@yahoo.com.hk",
                  "currency" : "THB",
                  "amount" : "226538.24",
                  "receiverAccountNumber" : "2472358560",
                  "receiverBankName" : "Siam Commercial Bank Public Company Limited (SCB)",
                  "receiverName" : "Kali Ho",
                  "orderingCustomerIdentificationNumber" : "Z169691(3)",
                  "orderingCustomerName" : "Wong Chun Pong"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.586Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:50.690Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN41951002",
              "_id" : "5ba4e8eee56846590939225f"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN55822283",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8e8e56846590939225a",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:44.765Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.188Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e1c"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.582Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e38"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "002",
                  "paymentReferenceNumber" : "IN55822283",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "66839415423",
                  "receiverEmail" : "Nutcharitt@yahoo.com",
                  "currency" : "THB",
                  "amount" : "11809.42",
                  "receiverAccountNumber" : "7900156865",
                  "receiverBankName" : "Bangkok Bank Public Company Limited (BBL)",
                  "receiverName" : "Nutcha Ritthikitticharoen",
                  "orderingCustomerIdentificationNumber" : "G3195836P",
                  "orderingCustomerName" : "Nutcha Ritthikitticharoen"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.582Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:44.765Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN55822283",
              "_id" : "5ba4e8e8e568465909392259"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN59859953",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "Delivered",
              "cash_out_status" : 4,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN63309694",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8f2e568465909392264",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:54.710Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.199Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e26"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.588Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3d"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "004",
                  "paymentReferenceNumber" : "IN63309694",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "660894343358",
                  "receiverEmail" : "Kp.passaporn@yahoo.co.th",
                  "currency" : "THB",
                  "amount" : "28245.27",
                  "receiverAccountNumber" : "0462003279",
                  "receiverBankName" : "Kasikorn Bank Public Company Limited (KBANK)",
                  "receiverName" : "Passaporn Suriwong",
                  "orderingCustomerIdentificationNumber" : "G3198848P",
                  "orderingCustomerName" : "Passaporn Suriwong"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.588Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:54.710Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN63309694",
              "_id" : "5ba4e8f2e568465909392263"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN71881605",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8ece56846590939225e",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:48.683Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.193Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e20"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.585Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3a"
                  }
              ],
              "store" : {
                  "purposeCode" : "318134",
                  "fiCode" : "004",
                  "paymentReferenceNumber" : "IN71881605",
                  "purposeDescription" : "For payment of exported goods",
                  "receiverMobileNumber" : "66962465492",
                  "receiverEmail" : "ejsinphuket@gmail.com",
                  "currency" : "THB",
                  "amount" : "38000",
                  "receiverAccountNumber" : "0138589781",
                  "receiverBankName" : "Kasikorn Bank Public Company Limited (KBANK)",
                  "receiverName" : "Patitta Traiwet",
                  "orderingCustomerIdentificationNumber" : "2287132",
                  "orderingCustomerName" : "Trip Guru Limited"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.585Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:48.683Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN71881605",
              "_id" : "5ba4e8ece56846590939225d"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN72381123",
  "latestResponse" : {
      "response" : {
          "sanima_response" : {
              "AGENT_TXNID" : "IN72381123",
              "STATUS_DATE" : "",
              "STATUS" : "UN-PAID",
              "PAYOUTCURRENCY" : "NPR",
              "PAYOUTAMT" : "16685.4800",
              "RECEIVER_NAME" : "PURNA BAHADUR CHAUHAN",
              "SENDER_NAME" : "PURNA BAHADUR CHAUHAN",
              "PINNO" : "44117199180",
              "MESSAGE" : "TXN Summary",
              "AGENT_SESSION_ID" : "IN72381123",
              "CODE" : "0"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN75349554",
  "latestResponse" : {
      "response" : {
          "data" : {
              "mapped_status" : "ERROR",
              "datainfo" : "Suspicious Transaction",
              "message" : "FAILED",
              "status" : 220
          },
          "success" : false
      }
  }
}

,
{
  "TransactionId" : "IN80699223",
  "latestResponse" : {
      "response" : {
          "data" : {
              "getRemittanceStatusResponse" : {
                  "requestReferenceNo" : "IN80699223",
                  "reviewStatus" : {
                      "itemsForReview" : "",
                      "reviewRequired" : "false"
                  },
                  "transactionStatus" : {
                      "beneficiaryReferenceNo" : "",
                      "bankReferenceNo" : "826218202438",
                      "subStatusCode" : "npci:E91",
                      "statusCode" : "SENT_TO_BENEFICIARY"
                  },
                  "transferCurrencyCode" : "INR",
                  "transferAmount" : "10493.6",
                  "transactionDate" : "2018-09-19T18:43:27",
                  "reqTransferType" : "ANY",
                  "transferType" : "IMPS",
                  "version" : "1.0"
              }
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN88578854",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "On Hold",
              "cash_out_status" : 5,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN89871294",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8eae56846590939225c",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:46.809Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.191Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e1e"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.584Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e39"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "014",
                  "paymentReferenceNumber" : "IN89871294",
                  "purposeDescription" : "Gift - Family Maintenance",
                  "receiverMobileNumber" : "66950157060",
                  "receiverEmail" : "chiwseechoon@yahoo.co.uk",
                  "currency" : "THB",
                  "amount" : "9343.6",
                  "receiverAccountNumber" : "9662261656",
                  "receiverBankName" : "Siam Commercial Bank Public Company Limited (SCB)",
                  "receiverName" : "ANTRALOK SOMJIT",
                  "orderingCustomerIdentificationNumber" : "750430035143",
                  "orderingCustomerName" : "CHIW SEE CHOON"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.584Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:46.809Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN89871294",
              "_id" : "5ba4e8eae56846590939225b"
          },
          "success" : true
      }
  }
}

,
{
  "TransactionId" : "IN96955827",
  "latestResponse" : {
      "response" : {
          "success" : true,
          "payment" : {
              "description" : "Delivered",
              "cash_out_status" : 4,
              "status" : 0
          }
      }
  }
}

,
{
  "TransactionId" : "IN99189679",
  "latestResponse" : {
      "response" : {
          "payload" : {
              "statusLogs" : [ 
                  {
                      "_id" : "5ba4e8f6e568465909392268",
                      "remarks" : "pending -> kasikorn",
                      "status" : "SAVED",
                      "timestamp" : "2018-09-21T12:49:58.725Z"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:57:59.203Z",
                      "status" : "PAYLIST",
                      "remarks" : "Added to Paylist : Paylist_00077390_2018092104",
                      "_id" : "5ba54d47a8e3b8459b8a1e2a"
                  }, 
                  {
                      "timestamp" : "2018-09-21T19:58:04.590Z",
                      "status" : "SENT",
                      "remarks" : "filename : Paylist_00077390_2018092104",
                      "_id" : "5ba54d4ca8e3b8459b8a1e3f"
                  }
              ],
              "store" : {
                  "purposeCode" : "318052",
                  "fiCode" : "017",
                  "paymentReferenceNumber" : "IN99189679",
                  "purposeDescription" : "Gift - Transfer to own account",
                  "receiverMobileNumber" : "66812664213",
                  "receiverEmail" : "btlau@hotmail.com",
                  "currency" : "THB",
                  "amount" : "218112.05",
                  "receiverAccountNumber" : "5401033337",
                  "receiverBankName" : "Citibank N.A. (CITI)",
                  "receiverName" : "Lau Boon Tuan",
                  "orderingCustomerIdentificationNumber" : "690507045453",
                  "orderingCustomerName" : "Lau Boon Kok"
              },
              "__v" : 0,
              "receivedFromKasikornTimestamp" : null,
              "sentToKasikornTimestamp" : "2018-09-21T19:58:04.590Z",
              "requestCreatedTimestamp" : "2018-09-21T12:49:58.725Z",
              "currentStatus" : "SENT",
              "paymentReferenceNumber" : "IN99189679",
              "_id" : "5ba4e8f6e568465909392267"
          },
          "success" : true
      }
  }
}]
console.log(o)
jsonToCSV(o, fileName)
  .then((a) => {
    // success
    console.log(a, 'SUCCESS')
  })
  .catch(error => {
    // handle error
    console.log(error);
  })