const Client = require('ssh2-sftp-client');
const co = require('co');
const fs = require('fs');
const path = require('path');

const FILENAME = '';
const INSTAREM_PVT_KEY = 'instarem'
const SFTP_OBJECT = {
  enabled: true,
  pki: true,
  url: 'tests.asdas.com',
  port: 923,
  username: 'instaremprocessing',
  password: '',
  from_local_folder: 'Encrypted',
  // in to the sftp server folder
  to_server_folder: '/incoming',
  // out file from sftp server folder
  from_server_folder: '/outgoing',
  to_local_folder: '/Reconciliation',
  saved_file_extension: 'recon',
};

const SSH_OBJECT = {
  symmetric: {
    enabled: false,
    encryption_key: undefined,
  },
  asymmetric: {
    enabled: true,
    public_key: 'instarem.pub',
    instarem_private_key: 'instarem',
  },
  encrypted_file_extension: '.pgp',
  encrypted_folder: 'Encrypted',
  decrypted_folder: 'Decrypted',
};

function getConnectionObject(lSFTPConfig) {
  let sshPrivateKey = ''
  const sshPassphrase = lSFTPConfig.password
  if (sshPassphrase === null) {
    const lConfigError = { name: 'Config Error', error: 'SFTP Pass phrase not set' }
    throw lConfigError
  }

  if (lSFTPConfig.pki === true) {
    const lEncrypt = SSH_OBJECT;
    if (lEncrypt.asymmetric.enabled === true) {
      sshPrivateKey = require('fs').readFileSync(path.resolve(`${__dirname}/${lEncrypt.asymmetric.instarem_private_key}`), 'utf-8')
    }
  }

  // ssh login for UAT and Production

  return {
    host: lSFTPConfig.url,
    port: lSFTPConfig.port,
    username: lSFTPConfig.username,
    privateKey: sshPrivateKey,
    password: lSFTPConfig.password,
  }
}

function* getSFTPObject(lSFTPConfig) {
  try {
    const sftp = new Client()
    const connectionObject = getConnectionObject(lSFTPConfig)
    yield sftp.connect(connectionObject)
    logger.info(`SFTP_CONNECTED-${connectionObject.host}:${connectionObject.port}`)
    return sftp
  } catch (e) {
    console.log(e)
  }
}

function fetchDataFromStreamAndSaveToDisk(stream, writeFilePath) {
  const writeStream = fs.writeFileSync(stream)

  return new Promise((resolve, reject) => {
    // This is here incase any errors occur
    writeStream.on('error', (err) => {
      reject(err)
      logger.child({ function: 'fetchDataFromStreamAndSaveToDisk' }).error(err)
    })
    stream.pipe(writeStream)
    stream.on('end', () => {
      logger.debug('Stream Closed')
      resolve(true)
    })
  })
}

function* saveFileToLocalDisk(stream, filename, folder) {
  try {
    return yield fetchDataFromStreamAndSaveToDisk(stream, path.resolve(`${folder}/${filename}`))
  } catch (error) {
    if (error.message === 'No such file') {
      console.log(error)
    }
    console.log(error)
  }
}

function* startReconcile() {
  const lSFTPConfig = SFTP_OBJECT

  const sftp = yield getSFTPObject(lSFTPConfig)
  console.log(sftp)
  const stream = yield sftp.get(`${lSFTPConfig.from_server_folder}/${FILENAME}`, true, 'utf8')
  yield saveFileToLocalDisk(stream, `encryptedFile${FILENAME}`, 'encrypted')
  return true;
}

co(startReconcile()).then((result) => {
  console.log(result)
}).catch((err) => {
  console.log(err)
})