const winston = require('winston');

var config = winston.config;

var logger = new(winston.Logger)({

  transports: [
    new(winston.transports.Console)({
      level: process.env.LOGGER_LEVEL || 'info',
      label: 'insta_test',
      timestamp: function () {
        return Date.now();
      },
      formatter: function (options) {
        return options.label + ' ' + config.colorize(options.level, options.level.toUpperCase()) + ' ' +
          new Date(options.timestamp()).toISOString() + ' ' +
          (options.message ? options.message : '') +
          (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
      }
    })
  ]
});

logger.error(new Error('test'))
  // logger.error('errror %s', errorAsJSON(new Error('test')))