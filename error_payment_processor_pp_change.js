
/** Transaction Allow reprocessing */

db.getCollection('transactions').update(
  {
      "store.transaction_id" : { $in: ["IN32791058", "IN22126097", "IN03723636"] } , 
      "current_status": 'MANUAL_PG', 
      "api_provider" : "danamonBank", 
  },
  { "$set": {
          "current_status": "ERROR",
          "last_updated_status": "ERROR",
          "api_provider": 'xendit',
          "manual_pg": false,
      },
    "$push": {
      'status_log': {
         $each: [{
          "updated_at" : ISODate(),
          "created_at" : ISODate(),
          "status_code" : 200,
          "status" : "ERROR",
          "bank_reference_number" : "",
          "description" : "payment in error",
          "error_code" : "NA",
          "error_status" : "NA",
          "error_description" : "payment in error",
          "api_provider" : "xendit"
         }],
         $position: 0
      }
    }
  })


  /****************************************************************************************/


  