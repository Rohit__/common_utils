const postgres = require('pg-promise');
const initOptions = {
  disconnect(client, dc) {
    const cp = client.connectionParameters;
    console.log('Disconnecting from database:', cp.database);
  },
  connect(client, dc, isFresh) {
    const cp = client.connectionParameters;
    console.log('Connected to database:', cp.database);
  }
}
const newInstaFetchQuery = require('./austracFetchQuery');
const pgp = postgres(initOptions);
const cn = 'postgres://instarem_admin:InstaremAdmin1!@192.168.2.248:5432/instarem_development';
// const cn = 'postgres://readonly_user:123@localhost:5432/dvdrental';
const db = pgp(cn);
db.connect().then((db) => {
    return db.any(newInstaFetchQuery);
  })
  .then((resData) => {
    resData.forEach((response) => {
      const writeData = {
        transaction_id: response.transaction_id,
        transaction_number: response.transaction_number,
        transaction: {
          destination_currency: response.destination_currency,
          destination_amount: response.destination_amount,
          direction: response.destination_currency ? 'I' : (response.insta_entity_country_code === 'AU'),
          created_at: response.created_at,
          updated_at: response.updated_at,
        },
        beneficiary: {
          name: response.beneficiary_name,
          company_name: response.beneficiary_company_name,
          address1: response.beneficiary_address1,
          address2: response.beneficiary_address2,
          email: response.beneficiary_email,
          city: response.beneficiary_city,
          state: response.beneficiary_state,
          country_code: response.beneficiary_country_code,
          mobile_country_code: response.beneficiary_mobile_country_code,
          mobile_number: response.beneficiary_mobile_number,
          account_number: response.beneficiary_account_number,
          postcode: response.beneficiary_postcode,
          account_number: response.beneficiary_account_number,
          account_type: response.beneficiary_account_type,
          routing_code_type_1: response.routing_code_type_1,
          routing_code_type_2: response.routing_code_type_2,
          routing_code_type_3: response.routing_code_type_3,
          routing_code_value_1: response.routing_code_value_1,
          routing_code_value_2: response.routing_code_value_2,
          routing_code_value_3: response.routing_code_value_3,
        },
        beneficiary_bank: {
          bank_name: response.beneficiary_bank_name,
        },
        remitter: {
          country_code: response.remitter_country_code,
          account_type: response.remitter_account_type,

          corporate_name: response.remitter_corporate_name,
          corporate_phone_country_code: response.remitter_corporate_phone_country_code,
          corporate_phone_number: response.remitter_corporate_phone_number,
          corporate_mobile_number: response.remitter_corporate_mobile_number,
          corporate_mobile_country_code: response.remitter_corporate_mobile_country_code,
          corporate_address1: response.remitter_corporate_address1,
          corporate_address2: response.remitter_corporate_address2,
          corporate_suburb: response.remitter_corporate_suburb,
          corporate_city: response.remitter_corporate_city,
          corporate_state: response.remitter_corporate_state,
          corporate_postcode: response.remitter_corporate_postcode,
          individual_name: response.remitter_individual_name,
          individual_phone_country_code: response.remitter_individual_phone_country_code,
          individual_phone_number: response.remitter_individual_phone_number,
          individual_mobile_number: response.remitter_individual_mobile_number,
          individual_mobile_country_code: response.remitter_individual_mobile_country_code,
          individual_address1: response.remitter_individual_address1,
          individual_address2: response.remitter_individual_address2,
          individual_suburb: response.remitter_individual_suburb,
          individual_city: response.remitter_individual_city,
          individual_state: response.remitter_individual_state,
          individual_postcode: response.remitter_individual_postcode,
        },
        instarem_entity: {
          name: response.insta_entity_name,
          address1: response.insta_entity_address1,
          address2: response.insta_entity_address2,
          suburb: response.insta_entity_suburb,
          city: response.insta_entity_city,
          state: response.insta_entity_state,
          postcode: response.insta_entity_postcode,
          country_code: response.insta_entity_country_code,
        },
        gateway: {
          name: response.payment_gateway_name,
          address: response.payment_gateway_address,
          city: response.payment_gateway_city,
          state: response.payment_gateway_state,
          postcode: response.payment_gateway_postcode,
          country: response.payment_gateway_country,
        },
        status: response.status,
        sub_status: response.sub_status,
      }
      console.log(response);
      require('fs').writeFileSync('data.json', JSON.stringify(writeData), { encoding: 'utf8' })
    })
  }).catch(e => {
    console.log(e);
  })


/*
    transaction_id: { type: Number, required: true },
    transaction: {
      transaction_number: { type: String, required: true },
      destination_currency: { type: String, required: true },
      destination_amount: { type: Number, required: true},
      direction: { type: String, required: true },
      created_at: { type: Date, required: true },
      updated_at: { type: Date, required: true },
    },
    beneficiary: {
      name: String,
      company_name: String,
      address1: { type: String, required: true },
      address2: String,
      email: String,
      city: { type: String, required: true },
      state: { type: String, required: true },
      country_code: { type: String, required: true },
      mobile_country_code: String,
      mobile_number: String,
      account_type: { type: String, required: true },
      account_number: { type: String, required: true },
      postcode: { type: String, required: true },
      routing_code_type_1: String,
      routing_code_type_2: String,
      routing_code_type_3: String,
      routing_code_value_1: String,
      routing_code_value_2: String,
      routing_code_value_3: String,
    },
    beneficiary_bank: {
      bank_name: { type: String, required: true },
    },
    remitter: {
      corporate_name: String,
      country_code: { type: String, required: true },
      account_type: { type: String, required: true },
      corporate_phone_country_code: String,
      corporate_phone_number: String,
      corporate_mobile_number: String,
      corporate_mobile_country_code: String,
      corporate_address1: String,
      corporate_address2: String,
      corporate_suburb: String,
      corporate_city: String,
      corporate_state: String,
      corporate_postcode: String,
      individual_name: String,
      individual_phone_country_code: String,
      individual_phone_number: String,
      individual_mobile_number: String,
      individual_mobile_country_code: String,
      individual_address1: String,
      individual_address2: String,
      individual_suburb: String,
      individual_city: String,
      individual_state: String,
      individual_postcode: String,
    },
    instarem_entity: {
      name: { required: true, type: String },
      address1:  { required: true, type: String },
      address2: String,
      suburb:  { required: true, type: String },
      city:  { required: true, type: String },
      state:  { required: true, type: String },
      postcode:  { required: true, type: String },
      country_code:  { required: true, type: String },
    },
    gateway: {
      name:   { required: true, type: String },
      address:  { required: true, type: String },
      city:  { required: true, type: String },
      state:  { required: true, type: String },
      postcode:  { required: true, type: String },
      country:  { required: true, type: String },
    },
    status:  { required: true, type: String},
    sub_status:  {required: true, type: String },
    is_sent: Boolean,
    xml_generation_err: Boolean,
*/