'use strict'

const json = {
  "a": ["A", "A", { "b": "B" }],
  "c": { "d": "D", "e": ["E", "E"] },
  "f": "F"
}

function isPrimitive(obj) {
  if (null === obj) return true;
  if (undefined === obj) return true;
  if (['string', 'number', 'boolean'].some(type => type === typeof true)) return true;
  return false;
}

function visit(obj, visitor) {
  for (let key of Object.keys(obj)) {
    if (isPrimitive(obj[key])) {
      visitor(key, obj[key]);
    } else if (Array.isArray(obj[key])) {
      for (let item of obj[key]) {
        visit(item, visitor);
      }
    } else {
      visit(obj[key], visitor);
    }
  }
}

const out = [];

visit(json, (k, v) => out.push(v));

console.log(out);
