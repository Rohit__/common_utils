//MASSPAY
//QUERY 1 - By paid Date
db.getCollection('transactions').aggregate([
  { $match: { "status": "PAID" } },
  {
    $project: {
      paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
      createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
      "local_conversion_currency": "$store.local_conversion_currency",
      'destination_currency': "$store.destination_currency",
      'destination_amount': "$store.destination_amount",
      "USD_Value": "$store.USD_Value"
    }
  },
  {
    $group: {
      _id: {
        paidDate: "$paidDate",
        destination_currency: "$destination_currency",
      },
      destination_amt: { "$sum": "$destination_amount" },
      count: { "$sum": 1 },
      USDVALUE: { "$sum": "$USD_Value" }
    }
  }
])

//QUERY 2 - BY created date
db.getCollection('transactions').aggregate([
  { $match: { "status": "PAID" } },
  {
    $project: {
      paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
      createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
      "local_conversion_currency": "$store.local_conversion_currency",
      'destination_currency': "$store.destination_currency",
      'destination_amount': "$store.destination_amount",
      "USD_Value": "$store.USD_Value"
    }
  },
  {
    $group: {
      _id: {
        createdDate: "$createdDate",
        destination_currency: "$destination_currency",
      },
      destination_amt: { "$sum": "$destination_amount" },
      count: { "$sum": 1 },
      USDVALUE: { "$sum": "$USD_Value" }
    }
  }
])

//QUERY 3 - ALL TRANSACTIONS BY created at
db.getCollection('transactions').aggregate([{
    $lookup: {
      from: "clients",
      localField: "client",
      foreignField: "_id",
      as: "client"
    }
  }, {
    $project: {
      paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
      'client': '$client.client_label',
      createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
      "local_conversion_currency": "$store.local_conversion_currency",
      'destination_currency': "$store.destination_currency",
      'destination_amount': "$store.destination_amount",
      "USD_Value": "$store.USD_Value"
    }
  },
  {
    $group: {
      _id: {
        createdDate: "$createdDate",
        destination_currency: "$destination_currency",
      },
      clientName: '$client',
      destination_amt: { "$sum": "$destination_amount" },
      count: { "$sum": 1 },
      USDVALUE: { "$sum": "$USD_Value" }
    }
  }
])


// QUERY 4  with client
db.getCollection('transactions').aggregate([{
    $lookup: {
      from: "clients",
      localField: "client",
      foreignField: "_id",
      as: "client"
    }
  }, {
    $project: {
      paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
      'client': '$client.client_label',
      createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
      "local_conversion_currency": "$store.local_conversion_currency",
      'destination_currency': "$store.destination_currency",
      'destination_amount': "$store.destination_amount",
      "USD_Value": "$store.USD_Value"
    }
  },
  {
    $group: {
      _id: {
        createdDate: "$createdDate",
        destination_currency: "$destination_currency",
      },
      clientName: { $first: '$client' },
      destination_amt: { "$sum": "$destination_amount" },
      count: { "$sum": 1 },
      USDVALUE: { "$sum": "$USD_Value" }
    }
  }
])



db.getCollection('transactions').aggregate([
  { $match: { 
      "status": "PAID",
      "status_logs": {
        $elemMatch: {
            status: {
              $in: ['AUTO_APPROVED', 'STAGE_2_APPROVED']
            }
        },
      },
    }
  },
  {
    $project: {
      paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
      createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
      "local_conversion_currency": "$store.local_conversion_currency",
      'destination_currency': "$store.destination_currency",
      'destination_amount': "$store.destination_amount",
      "USD_Value": "$store.USD_Value"
    }
  },
  {
    $group: {
      _id: {
        paidDate: "$paidDate",
        destination_currency: "$destination_currency",
      },
      destination_amt: { "$sum": "$destination_amount" },
      count: { "$sum": 1 },
      USDVALUE: { "$sum": "$USD_Value" }
    }
  }
])


// QUERY 5 - NEW Query with client
db.getCollection('transactions').aggregate([
  {
    $match: {
      "status": {$in: ["PAID" , "SENT_TO_BANK"]},
    }
},
{
  $lookup: {
    from: "clients",
    localField: "client",
    foreignField: "_id",
    as: "client"
  }
},
{
  $unwind: "$status_logs"
},
{
  "$match": {
   "status_logs.status": {
      $in: ['AUTO_APPROVED', 'STAGE_2_APPROVED']
    }
  }
},
{
$project: {
   paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
   'client': '$client.client_label',
    'approved_date':   { $dateToString: { format: "%Y-%m-%d", date: "$status_logs.created_at" } },
    'approved_status': '$status_logs.status',
   createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
   "local_conversion_currency": "$store.local_conversion_currency",
   'destination_currency': "$store.destination_currency",
   'destination_amount': "$store.destination_amount",
   "USD_Value": "$USD_Value"
  }
},
{
$unwind: "$client"
},
{
  $group: {
    _id: {
        "approved_date": "$approved_date",
         destination_currency: "$destination_currency",
         clientName:"$client"
    },
    destination_amt: { "$sum": "$destination_amount" },
    count: { "$sum": 1 },
    USDVALUE: { "$sum": "$USD_Value" }
  }
}
])