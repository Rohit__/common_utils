const postgres = require('pg-promise');
const initOptions = {
  disconnect(client, dc) {
    const cp = client.connectionParameters;
    console.log('Disconnecting from database:', cp.database);
  },
  connect(client, dc, isFresh) {
    const cp = client.connectionParameters;
    console.log('Connected to database:', cp.database);
  }
}
const newInstaFetchQuery = require('./austracFetchQuery');
const pgp = postgres(initOptions);
const cn = 'postgres://instarem_admin:InstaremAdmin1!@192.168.2.131:5432/instarem_migration';
// const cn = 'postgres://readonly_user:123@localhost:5432/dvdrental';
const db = pgp(cn);
db.connect().then((db) => {
    return db.any(newInstaFetchQuery);
  })
  .then((dta) => {
    console.log(dta);
  }).catch(e => {
    console.log(e);
  })