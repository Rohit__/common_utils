const Pool = require('concurrent-request')
const opts = {
  interval: (count) => Math.pow(2, count) * 1000, // exponentional backoff
  jitter: 1000, // 1 second range of jitter (+/- .5 seconds)
  size: 5, // At most 5 active connections
  tries: 5, // Give up after retrying 5 times
  handler: function (e, resp, body, cb) {
    if(e) { return cb(e); } // Retry if connection fails
    if(resp.code === 429) { return cb(new Error('rate limited')); } // retry
    return cb(null); // The request succeeded
  }
}
var request = new Pool(opts);
 
request({ 
    uri: 'http://example.foo', 
    body: 
    method: 'GET', json: true }, function (e, body) {
  if(e) {
    return e.forEach((err) => console.error(err));
  }
  console.log(JSON.stringify(body));
});