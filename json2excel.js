var XLSX = require('xlsx');
var XLSX_S = require('xlsx-style');
var moment = require('moment');
var fs = require('fs');
// const xl = require('excel4node');
setTimeout(() => {
  // var jsonToCSV = require('json-to-csv');
  // const fileName = './idr_banks.csv';

  // jsonToCSV(o, fileName)
  //   .then(() => {
  //     // success
  //   })
  //   .catch(error => {
  //     // handle error
  //     console.log(error);
  //   })
  // const SheetNames = ['January']

  // var workbookN = XLSX.readFile('./MSB_Sample.xlsx', { cellStyles: true });
  var workbookN = XLSX.readFile('./Excell.xlsx', { cellStyles: true });

  // const reportData = {
  //   A14: "test data",
  //   A15: "test data",
  //   A16: "test data",
  //   B17: "test data",
  //   B18: "test data",
  //   B19: "test data",
  // };

  // // const new_workBook = { SheetNames, Sheets: { January: reportData } }
  // const new_workBook = Object.assign({}, workbook);
  // const wopts = { bookType: 'xlsx', bookSST: false, type: 'base64' };
  // new_workBook.SheetNames[0] = 'December';
  // new_workBook.Sheets.December = Object.assign({}, new_workBook.Sheets.November);
  // // delete new_workBook.Sheets.November;
  // const filename = 'msb_December.xlsx';
  // // console.log(new_workBook)


  try {

    const SheetName = moment().format('MMMM');
    const workBookNew = XLSX.utils.book_new();
    workBookNew.SheetNames.push(SheetName);
    console.log('Try')
    const header_row_8 = [
      'Transaction Type',
      'Reporting Outlet ID',
      'State Code (only for POS and DFIs)',
      'Customer Details',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      'Transaction Details',
      ''
    ];
    const header_row_9 = [
      '', '', '',
      'Entity Type of Customer',
      'BRIC',
      'Name',
      '',
      'Address',
      '',
      'Postcode',
      'Town/City',
      'State',
      'Country',
      'Nationality',
      'Type of E-channel',
      'Transaction Date (DD/MM/YYYY)',
      'Name of Counterparty',
      'Relationship with Customer',
      'Unique Reference Number',
      'Country Code',
      'Purpose Code and Description',
      'Currency Code',
      'Volume',
      'Remittance Amount (FC)',
      'Remittance Amount (RM)'
    ]
    var ws = XLSX.utils.aoa_to_sheet([
      ['Name', 'InstaRem'],
      ['BRIC', 16568],
      ['Reporting Year', new Date().getFullYear()],
      ['Reporting Month', moment().format('MM')],
      [],
      [],
      [],
      header_row_8,
      header_row_9, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20]
    ]);


    ws['!merges'] = [XLSX.utils.decode_range("A8:A9"),
      XLSX.utils.decode_range("B8:B9"),
      XLSX.utils.decode_range("C8:C9"),
      XLSX.utils.decode_range("C8:C9"),
      XLSX.utils.decode_range("D8:N8"),
      XLSX.utils.decode_range("O8:Y8")
    ];
    ws['!margins'] = { left: 1.0, right: 1.0, top: 1.0, bottom: 1.0 };
    var wscols = [
      { wch: 17 },
      { wch: 19 },
      { wch: 34 },
      { wch: 21 },
      { wch: 23 },
      { wch: 6 },
      { wch: 6 },
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 7 },
      { wch: 10 },
      { wch: 10 },
      { wch: 10 },
      { wch: 12 },
      { wch: 10 },
      { wch: 10 },
      { wch: 17 },
      { wch: 30 },
      { wch: 20 },
      { wch: 16 },
      { wch: 13 },
      { wch: 11 },
      { wch: 28 },
      { wch: 13 },
      { wch: 6 },
      { wch: 22 },
      { wch: 22 }
    ];

    /* 1 to 7 rows */
    var wsrows = [, , , , , , , { hpx: 24, level: 3 }, { hpx: 24, level: 3 }];
    ws['!cols'] = wscols;

    /* TEST: row props */
    ws['!rows'] = wsrows;
    workBookNew.Sheets[SheetName] = ws;

    var defaultCellStyle = { font: { name: "Verdana", sz: 11 }, alignment: { "horizontal": "center" } };
    var wbout = XLSX_S.write(workBookNew, { bookType: 'xlsx', type: 'buffer', defaultCellStyle });
    fs.writeFileSync('./etestset.xlsx', Buffer.from(wbout).toString())

    console.log(wbout)
  } catch (e) {
    console.log(e);
  }
}, 5000)

// var wbss = {
//   SheetNames: ["Sheet1"],
//   Sheets: {
//     Sheet1: {
//       "!ref": "A1:C1",
//       A1: { t: "s", v: "'00010000", w: "'0010000", v: '00001000' }, // <-- General format
//       B1: { t: "n", v: 10000, z: "0%" }, // <-- Builtin format
//       C1: { t: "s", v: "0010000", z: "#" } // <-- Custom format
//     }
//   }
// }

// const aasdasd = XLSX.writeFile(wbss, 'sample.xlsx', { bookType: 'xlsx', type: 'binary' });
// console.log(aasdasd);