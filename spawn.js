const spawn = require('child_process').spawn
const gpg = spawn(`${__dirname}/decrypt.sh`, [
  path.join(`${__dirname}/../batch_files/${config.local_outbound}/${input}`),
  path.join(`${__dirname}/../batch_files/${encryptionAlgo.encrypted_folder}/${output}`),
  password,
])