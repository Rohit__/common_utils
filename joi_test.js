var joi = require('joi');
var Joi = require('joi');

const error = joi.validate('', joi.object().keys({

  destination_currency: 'HKD',
  DrRefNmbr: joi.string().optional(),
  bene: joi.object().keys({
    beneficiary_country_code: joi.string(),
    routing_code_type_1: joi.string().allow('').optional()
      .label('Routing Code Type 1')
      .when('beneficiary_country_code', {
        is: joi.valid('BN'),
        then: joi.allow('').optional(),
        otherwise: joi.optional()
      }),
    routing_code_value_1: joi.string()
      .label('Routing Code Value 1')
      .when('beneficiary_country_code', {
        is: joi.valid(['BN']),
        then: joi.allow('').optional(),
        otherwise: joi.optional()
      })
  })
}).required().label('Purpose Code').length(2).allow(''))





const simpleErr = joi.validate({ code: ' ', type: 'test' }, joi.object().keys({
  code: joi.string().required(),
  type: joi.string().required().error(() => 'Need Type'),
  value: joi.any().when('code', {
    is: joi.valid(['INDIVIDUAL_PERSONAL_IMAGE', 'UTILITY_BILL', 'OTHER_DOCUMENT']),
    then: joi.any(),
    otherwise: joi.string().required().error(() => 'Need Value')
  })
}), { abortEarly: false });

let schemas = joi.string().error(new Error('Was REALLY expecting a string'))
const errN = joi.validate(3, schemas);
// console.log(joi.validate([], joi.array().items(joi.string().optional()).optional()))

const aa = joi.validate('', joi.string().regex(/^[0-9]{10}$/).optional().allow(''));
console.log("aa", aa);


(() => {
  const schema = Joi.array().items(Joi.bool(), Joi.any().strip());

  schema.validate(['one', 'two', true, false, 1, 2], (erre, valuee) => {
    // console.log(valuee)
  });
})(Joi)

// console.log(JSON.stringify(simpleErr.error.message));
// console.log(error);
// console.log(errN)



const ifElseJoi = joi.object().keys({
  remitter_identification_type: joi.any()
    .when('remitter_country_code', {
      is: joi.valid(['AU']),
      then: joi.optional().allow(''),
      otherwise: joi.string().regex(/^[A-Za-z0-9\s-.]+$/ig).min(1).max(255)
        .label('Remitter Identification Type')
        .options({
          language: {
            string: {
              regex: {
                base: ':"{{!value}}" fails to match the required pattern.',
                name: ':"{{!value}}" fails to match the required pattern',
              },
            },
          },
        })
        .required(),
    })
}).unknown()

const whenOtherwise = joi.object().keys({
  beneficiary_address: Joi.any()
  .when('beneficiary_country_code', { 
    is: Joi.string().valid(['TH', 'IN']).required(),
    then: Joi.string().max(200).allow('').optional(), 
    otherwise: Joi.string().max(200).required() 
  }),
}).unknown()

console.log(joi.validate({ remitter_identification_type: '', beneficiary_country_code: 'TH', remitter_country_code: 'AU' }, whenOtherwise, { abortEarly: false }))