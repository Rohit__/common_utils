var MongoClient = require('mongodb').MongoClient,
  url = 'mongodb://localhost:27017/austrac_report',
  fs = require('fs');

MongoClient.connect(url, function (err, db) {
  const dbase = db.db('austrac_report');
  simplePipeline(dbase, function () {
    db.close();
  });
});

var simplePipeline = function (db, callback) {
  console.log(db)

  var collection = db.collection('transactions');
  collection.aggregate(
    [
      { $match: { "status": "PAID" } },
      {
        $project: {
          paidDate: { $dateToString: { format: "%Y-%m-%d", date: "$updated_at" } },
          createdDate: { $dateToString: { format: "%Y-%m-%d", date: "$created_at" } },
          "local_conversion_currency": "$store.local_conversion_currency",
          'destination_currency': "$store.destination_currency",
          'destination_amount': "$store.destination_amount",
          "USD_Value": "$store.USD_Value"
        }
      },
      {
        $group: {
          _id: {
            paidDate: "$paidDate",
            destination_currency: "$destination_currency",
          },
          destination_amt: { "$sum": "$destination_amount" },
          count: { "$sum": 1 },
          USDVALUE: { "$sum": "$USD_Value" }
        }
      }
    ],
    function (err, results) {

      console.log(results)
      fs.writeFileSync('query1.json', results)
      callback(results);
    }
  );
}