var heapdump = require('heapdump');

function aabbc(a, bbc) {
  console.log(a.b)
  a.b--;

  //  const limit = -5600;
  const limit = 0;
  if (a.b >= limit) {
    console.log(a.b);
    aabbc(a, bbc);
  } else {
    console.log(bbc(a.b))

  }
}

aabbc({
    a: { d: { a: { q: { s: { a: {} } } } }, a: { a: { b: { a: { c: {} } } } } },
    b: 5000
  },
  function (a) {
    console.log(a + ' reached')
    heapdump.writeSnapshot(function (err, filename) {
      console.log('dump written to', filename);
    });
  })